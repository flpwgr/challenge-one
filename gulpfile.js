// Gulp Plugins
var gulp 			= require("gulp");
var sass 			= require('gulp-sass');
var usemin 			= require('gulp-usemin');
var uglify 			= require('gulp-uglify');
var rev 			= require('gulp-rev');
var autoprefixer 	= require('gulp-autoprefixer');
var preprocess		= require('gulp-preprocess');
var rimraf			= require('gulp-rimraf');

// Not gulp exclusive dependencies
var browserSync = require('browser-sync').create();
var wiredep     = require('wiredep').stream;

gulp.task('wiredep', function(cb) {
	gulp.src('./app/index.html')
        .pipe(wiredep())
        .pipe(gulp.dest('./app'));

    return cb();
});

gulp.task('html', function(cb) {
	gulp.src('app/**/*.html')
		.pipe(gulp.dest('dist'));

	return cb();
});

// images
gulp.task('images', function(cb) {
	gulp.src('app/images/**/*')
		.pipe(gulp.dest('dist/images'));
	return cb();
});

gulp.task('bootstrap:fonts', function(cb) {
	var src = "./bower_components/font-awesome-sass/assets/fonts/**/*.*";
	var dst = "dist/fonts/";

	gulp.src(src).pipe(gulp.dest(dst));

	return cb();
})

gulp.task('serve', ['wiredep', 'sass'], function(cb) { 
	browserSync.init({
		server: {
			baseDir: 'app/',
			routes: {
				"/bower_components": "bower_components",
				"/fonts":"bower_components/font-awesome-sass/assets/fonts"
			}
		}
	});

	gulp.watch('app/scss/*.scss', ['sass']);
	gulp.watch('app/**/*.html').on('change', browserSync.reload);
	gulp.watch('app/js/**/*.js').on('change', browserSync.reload);

	return cb();
});

gulp.task('sass', function() {
	return gulp.src('app/scss/*.scss')
				.pipe(sass({
					includePaths:[
						"./bower_components/",
					]
				}))
				.pipe(autoprefixer())
				.pipe(gulp.dest("app/css"))
				.pipe(browserSync.stream());
});

gulp.task('usemin', ['wiredep', 'html', 'sass'], function(cb) {
     gulp.src('app/index.html')
         .pipe(usemin({
         	css: [rev()],
            jsDeps: [uglify(), rev()]
         }))
         .pipe(gulp.dest('dist'));

     return cb();
});

gulp.task('clean', function() {
	// rimraf('dist/js', cb);
	return gulp.src('dist/*', {read: false})
		.pipe(rimraf());
});

gulp.task('default', ['wiredep', 'serve']);
gulp.task('build', ['clean'], function(){
	gulp.start(['wiredep', 'html', 'sass', 'images','bootstrap:fonts', 'usemin']);
})