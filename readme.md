# Code Challenge

# Preparing..

You should have gulp installed globally on your machine
```npm install -g gulp```

Then checkout the project and inside the directory:

```npm install```

and

```bower install```

You are ready to go !

# Running

just run ```gulp serve``` and the browser should popout with the challenge screen


# Considerations..

I createad a very simple file structure for this exercise.

Basically we have a entry point for the app logic (app.js), and all the controllers, services and filters are tied to this file.

On a bigger project, a better separation of the files will be necessary.


Any other questions and considerations regarding the app logic/strcture feel free to contact me.




