# Commands

```
curl --include \
     --request POST \
     --header "Content-Type: application/json" \
     --data-binary "{
    \"date\":\"now\"
}" \
'https://challenges.1aim.com/roombooking/getrooms'
```


```
curl --include \
     --request POST \
     --header "Content-Type: application/json" \
     --data-binary "{
    \"date\":\"1498129479\"
}" \
'https://challenges.1aim.com/roombooking/getrooms'
```