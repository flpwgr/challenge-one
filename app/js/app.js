var app = angular.module("rooms", ['ui.router','ngQuickDate','cgNotify']);

app.config( function($httpProvider, $stateProvider, $urlRouterProvider) {
  
    $httpProvider.defaults.headers.post['Content-Type'] = "text/plain";

    // Define states here
    $urlRouterProvider.otherwise('main');
    // MAIN PAGE
    $stateProvider
        .state('main', {
            url:'/main',
            templateUrl:'views/main.html',
            controller:'roomController',
            resolve: {
                rooms: ['roomsAPI', function(roomsAPI) {
                    return roomsAPI.list('now');
                }]
            }
        })
        .state('book', {
            url:'/book',
            templateUrl:'views/book.html',
            controller: 'bookController',
            params: {
                room: null,
                date: null
            }
        });
})
.controller('roomController', function($scope, $state, rooms, roomsAPI, timeSlotService) {
    $scope.rooms = rooms;
    $scope.errmsg = null;
    $scope.canCheckAvail = false;
    $scope.myDate = new Date();
    $scope.checkAvail = checkAvail;

    $scope.dateChange = function() {
        $scope.rooms = [];
        roomsAPI.list(Math.floor($scope.myDate.getTime()/100)).then(function(data) {
            $scope.rooms  = data;
        }, function(err) {
            $scope.errmsg = "Fail to retrieve data :(";
        });
    }
    $scope.dateFilter = function(d) {
        var today = new Date();

        return d > today-(60*60*24);
    }

    // This will send the room object to the next screen
    $scope.clickBook = function(room) {
        $state.go('book',{room: room,date: $scope.myDate});
        return true;
    }

    function checkAvail(room) {
        return !$scope.canCheckAvail || room.avail.length > 0;
    };
})
.controller('bookController', function($scope, $state, $filter, $stateParams, notify, roomsAPI) {
    if( !$stateParams.room ) {
        $state.go('main');
        return;
    } 

    var firstClick = false;
    var timeSlotCopy = null;
    $scope.waitFinish = false;
    $scope.err = {
        name: false,
        number: false,
        email: false
    };

    $scope.initHour = 7;
    $scope.initMin = 0;
    $scope.endHour = 7;
    $scope.endMin = 0;
    $scope.hours = [
        {value:7, label:"07"},
        {value:8, label:"08"},
        {value:9, label:"09"},
        {value:10,label:"10"},
        {value:11,label:"11"},
        {value:12,label:"12"},
        {value:13,label:"13"},
        {value:14,label:"14"},
        {value:15,label:"15"},
        {value:16,label:"16"},
        {value:17,label:"17"},
        {value:18,label:"18"},
    ]

    $scope.mins = [
        {value:0, label:"00"},
        {value:15,label:"15"},
        {value:30,label:"30"},
        {value:45,label:"45"}
    ];


    // Main functions
    $scope.addAtendee = addAtendee;
    $scope.removeAtendee = removeAtendee;
    $scope.clickBook = clickBook;
    $scope.checkTimeInterval = checkTimeInterval;
    $scope.clickTime = clickTime;
    
    // End Main functions

    $scope.user = {
        name: "",
        number: "",
        email: "",
    };
    //information about the booking
    $scope.book = {
        booking: { 
            date: null,
            time_start: null,
            time_end: null,
            title: "event title",
            description: "event description",
            room: null
        },
        passes: []
    }

    $scope.$watch("user.name", function(newValue,oldValue){
        $scope.err.name = false;
    });
    $scope.$watch("user.number", function(newValue,oldValue){
        $scope.err.number = false;
    })
    $scope.$watch("user.email", function(newValue,oldValue){
        $scope.err.email = false;
    })


    function checkTimeInterval() {
        // check if the time array is free between the selected range
        if( $scope.initHour==null || $scope.initMin==null || $scope.endHour == null || $scope.endMin == null ) {
            return false;
        }

        $scope.timeError = "";

        var initHour = $scope.initHour - 7;
        var initMin = ($scope.initMin==0)?0:$scope.initMin/15;

        var endHour = $scope.endHour - 7;
        var endMin = ($scope.endMin==0)?0:$scope.endMin/15;

        selectTimeInterval();

        for(var i = initHour ; i <= endHour ; i++ ){
            var j = 0;
            var jEnd = 3;

            // if is starting, get the quarter from initMin
            if( i == initHour ){
                j = initMin;
            }
            // if is the last run the max should be from endMin
            if( i == endHour ) {
                jEnd = endMin;
            }
            for(j ; j <= jEnd ; j++ ) {

                if( !timeSlotCopy[i][j] ) {
                    // not avaliable
                    $scope.timeError = "Already booked at this time";
                    return false;
                }
            }
        }
        return true;
    };

    function clickBook() {
        if( !checkTimeInterval() ) {
            // popup error
            notify("Cannont add this time interval");
            return false;
        }

        var year    = $scope.date.getFullYear();
        var month   = $scope.date.getMonth();
        var day     = $scope.date.getDate();

        var dInit = new Date(year,month, day, $scope.initHour, $scope.initMin);
        var dEnd = new Date(year,month, day, $scope.endHour, $scope.endMin);

        $scope.book.booking.date        = Math.floor($scope.date.getTime() / 1000);
        $scope.book.booking.time_start  = Math.floor(dInit.getTime() / 1000);
        $scope.book.booking.time_end    = Math.floor(dEnd.getTime() / 1000);

        $scope.waitFinish = true;
        roomsAPI.book($scope.book).then( function(data) {
            $scope.waitFinish = false;
            if(data.error) {
                notify('Error: '+data.error.text);        
            } else {
                notify('Reservation confirmed !');
                $state.go('main');
            }
        }, function(){
            $scope.waitFinish = false;
            notify('Failed to send data');
        });
    };

    function clickTime(hour, min) {
        firstClick = !firstClick;

        if( firstClick ) {
            $scope.room.timeSlot = angular.copy( timeSlotCopy );
            // we are clicking for the first time
            $scope.initHour = hour + 7;
            $scope.initMin  = min * 15;
            $scope.room.timeSlot[hour][min] = 2;
        } else {
            $scope.endHour  = hour + 7;
            $scope.endMin   = min * 15;   
            // selectTimeInterval();
            checkTimeInterval();
        }
    }

    function removeAtendee(index) {
        $scope.book.passes.splice(index, 1);
    };

    function resetErrorsAtendee() {
        $scope.err.name = false;
        $scope.err.email = false;
        $scope.err.number = false;    
    };

    function addAtendee() {
        resetErrorsAtendee();
        
        // to add a atendee we should validate the fields
        var name  = $scope.user.name;
        var email = $scope.user.email;
        var number = $scope.user.number;
        var error = false;

        if( isEmpty(name) ) {
            // error
            $scope.err.name = true;
            error = true;
        }
        if( isEmpty(email) ) {
            // error empty
            $scope.err.email = true;
            error = true;
        }
        if( isEmpty(number) ) {
            // error empty
            $scope.err.number = true;
            error = true;
        }
        if( !isValidPhoneNumber(number) ) {
            $scope.err.number = true;
            error = true;
        }

        if( !isValidEmail(email) ) {
            // error invalid
            $scope.err.email = true;
            error = true;
        }

        if(error) return false;

        $scope.book.passes.push({name:name,email:email,number:number})
        return true;
    };

    function selectTimeInterval() {
        $scope.room.timeSlot = angular.copy( timeSlotCopy );
        // check if the time array is free between the selected range
        if( $scope.initHour==null || $scope.initMin==null || $scope.endHour == null || $scope.endMin == null ) {
            return false;
        }

        var initHour = $scope.initHour - 7;
        var initMin = ($scope.initMin==0)?0:$scope.initMin/15;

        var endHour = $scope.endHour - 7;
        var endMin = ($scope.endMin==0)?0:$scope.endMin/15;

        for(var i = initHour ; i <= endHour ; i++ ){
            var j = 0;
            var jEnd = 3;

            // if is starting, get the quarter from initMin
            if( i == initHour ){
                j = initMin;
            }
            // if is the last run the max should be from endMin
            if( i == endHour ) {
                jEnd = endMin;
            }
            for(j ; j <= jEnd ; j++ ) {
                $scope.room.timeSlot[i][j] = 2;
            }
        }
    };

    $scope.room = $stateParams.room;
    $scope.date = $stateParams.date;
    $scope.book.booking.room = $scope.room.name;
    timeSlotCopy = angular.copy($scope.room.timeSlot);
})
.factory('timeSlotService', function() {
    // Convert intervals to slots of avaliable time
    // 7 8 9 10 11 12 13 14 15 16 17 18 19
    // 0 1 2 3  4  5  6  7  8  9  10 11 12
    return function intervalToTimeSlots(interval) {
        var arrayOffset = 7;
        var hour = [false,false,false,false];
        var timeSlot = [
            [false,false,false,false],
            [false,false,false,false],
            [false,false,false,false],
            [false,false,false,false],
            [false,false,false,false],
            [false,false,false,false],
            [false,false,false,false],
            [false,false,false,false],
            [false,false,false,false],
            [false,false,false,false],
            [false,false,false,false],
            [false,false,false,false]
        ];

        var nIntervals = interval.length;

        for(var i=0; i < nIntervals ; i++ ) {
            // analyze the string an parse it to the array...
            var avail = interval[i].split(" - ");
            var init = avail[0].split(":");
            var end = avail[1].split(":");

            var initHour = Number(init[0]);
            var initMin  = Number(init[1]);
            var endHour  = Number(end[0]);
            var endMin   = Number(end[1]);

            var openWindow = false;
            for(j=0; j < 12 ; j++) {
                if(openWindow) {
                    if( j+arrayOffset < endHour ) {
                        // parse time
                        for(var k=0; k < 4 ; k++){
                            timeSlot[j][k] = true;
                        }
                        continue;
                    }

                    if( j+arrayOffset == endHour ) {
                        if( endMin == 0) {
                            continue;
                        }

                        var min = endMin / 15;
                        for(var k=0; k <= min ; k++){
                            timeSlot[j][k] = true;
                        }
                    }
                }

                if( j+arrayOffset < initHour) {
                    continue;
                }
                if( j+arrayOffset > endHour) {
                    break;
                }

                if(j+arrayOffset == initHour) {
                    var min = initMin/15;
                    for(var k=min; k < 4 ; k++) {
                        timeSlot[j][k] = true;
                    }
                    openWindow = true;
                }
            }
        }
        return timeSlot;
    }
})
.factory('roomsAPI', function($q, $http, timeSlotService) {
    return {
        'list': function(date) {
            // show all
            // https://challenges.1aim.com/roombooking/getrooms
            var req = $http.post('https://challenges.1aim.com/roombooking/getrooms',{date:date});

            var deferred = $q.defer();
            req.then(function(data){
                data.data = data.data.map(function(item){
                    item.timeSlot = timeSlotService(item.avail);
                    return item;
                });
                deferred.resolve(data.data);
            })

            return deferred.promise;
        },
        'book': function(data) {
            // data is the required JSON
            return $http.post('https://challenges.1aim.com/roombooking/sendpass',data)
        }
    }
})
.filter('hour', function(){
    return function(hour) {
        return (hour < 10)? "0"+hour : hour;
    }
});


function isEmpty(str) {
    return str.trim().length == 0;
}
function isValidPhoneNumber(number) {
    var re = /^\+?[1-9]\d{1,14}$/;
    return re.test(number);
}
function isValidEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

